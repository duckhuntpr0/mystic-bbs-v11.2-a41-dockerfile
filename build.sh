#usr/bin/env bash

# Build Mystic BBS docker image and create docker volumes


IMAGE=duckhp/mystic_bbs:latest
DOCKER_VOLUME_HOME="mysticbbs-docker_home"
DOCKER_VOLUME_ETC="mysticbbs-docker_etc"

## creating volumes for '/home' and '/etc' if not alrady present

docker volume ls | grep $DOCKER_VOLUME_HOME 2>&1 >/dev/null
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
	echo "Creating docker volume"
	docker volume create $DOCKER_VOLUME_HOME
fi

docker volume ls | grep $DOCKER_VOLUME_ETC 2>&1 >/dev/null
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
	echo "Creating docker volume"
	docker volume create $DOCKER_VOLUME_ETC
fi


#docker volume inspect $DOCKER_VOLUME_ETC 2>&1 >/dev/null
#RETVAL=$?
#if [ $RETVAL -ne 0 ]
#then
#	echo "Creating docker volume $DOCKER_VOLUME_ETC"
#	docker volume create $DOCKER_VOLUME_ETC
#fi

#docker volume create $DOCKER_VOLUME_HOME
#docker volume create $DOCKER_VOLUME_ETC

docker build --rm -t $IMAGE .
