#usr/bin/env bash

DROPBEAR_SSH_PORT=2222
MYSTIC_TELNET_PORT=6023
MYSTIC_SSH_PORT=6022

#PWD=$(pwd)
IMAGE=duckhp/mystic_bbs:latest
DOCKER_CONTAINER_NAME="MysticBBS"
DOCKER_VOLUME_HOME="mysticbbs-docker_home"
DOCKER_VOLUME_ETC="mysticbbs-docker_etc"

docker run -d -ti -tiu root \
	-v $DOCKER_VOLUME_ETC:/etc \
	-v $DOCKER_VOLUME_HOME:/home \
	-p $DROPBEAR_SSH_PORT:2222 \
	-p $MYSTIC_TELNET_PORT:23 \
	-p $MYSTIC_SSH_PORT:22 \
	--name=$DOCKER_CONTAINER_NAME $IMAGE
