# [Mystic BBS](http://mysticbbs.com) (11.2 alpha41 based) Dockerfile
<i>Disclaimer: Maintainer of this Dockerfile is <b>NOT</b> affiliated with the authors of [Mystic BBS](http://mysticbbs.com/) or [Cryptlib](https://www.cs.auckland.ac.nz/~pgut001/cryptlib/). Please see http://wiki.mysticbbs.com for official [Mystic BBS](http://mysticbbs.com/) documentation.</i>

<hr/>

- [Repo at BitBucket](https://bitbucket.org/duckhuntpr0/mystic-bbs-v11.2-a41-dockerfile)
- [Docker Hub Repo](https://hub.docker.com/r/duckhp/mystic_bbs)


<hr/>

## To build and run the Docker solution:

<b>(please inspect the two scripts beforehand)</b>

<pre>./build.sh && ./run.sh</pre>
* This builds and runs the docker in detached mode, and it should be possible to ssh into the dropbear server running on port 2222, with the username 'mysticbbs' and default password 'mystic'. Confirm that it's running with 'docker ps'.
  * Tip: Use 'ssh <b>-o UserKnownHostsFile=/dev/null</b> 127.0.0.1 -l mysticbbs -p 2222' to prevent storing lots of host keys during testing.

* '/etc and '/home' are placed in docker volumes for persistence of data.
  * <b>Be careful about changing that</b>, since the [boot script](https://github.com/DuckHP/mystic_bbs_docker/blob/4f42035ec14670fb9f23950845d37dbe9daf6c5d/src/boot.sh#L10), as it is at the moment, might extract it's [contents](https://github.com/DuckHP/mystic_bbs_docker/blob/master/src/tar_files_list.txt) over any mounts or volumes you may attach to the container, should either '/etc/passwd' or '/home' be found missing during 'boot.sh' execution.
  
* The default password for 'mysticbbs' user will be asked to be changed at first ssh login by [.ssh_login.sh](https://github.com/DuckHP/mystic_bbs_docker/blob/master/src/ssh_login.sh)
* Mystic BBS binaries and data can be found in ~/mystic.
* The docker is utilizing [tmux](https://github.com/tmux/tmux). See this [handy tmux cheat-sheet](https://tmuxcheatsheet.com/)

### For first time sysop account creation and administration:
<pre>$ cd ~/mystic/</pre>
* <small>Refer to http://wiki.mysticbbs.com/doku.php?id=installation </small>
<pre>$ ./mystic -l</pre>
* <small>Refer to http://wiki.mysticbbs.com/doku.php?id=config_howto </small>
<pre>$ ./mystic -cfg</pre>


#### Notable links
- [Docker Hub Repository](https://cloud.docker.com/repository/docker/duckhp/mystic_bbs)
- [Mystic BBS](http://mysticbbs.com/)
	- [wiki.mysticbbs.com](http://wiki.mysticbbs.com)
- [Cryptlib](https://www.cs.auckland.ac.nz/~pgut001/cryptlib)
- [supervisord](http://supervisord.org)
- [tmux](https://github.com/tmux/tmux)
- [dosemu](http://www.dosemu.org)
	- [dosemu README](http://www.dosemu.org/docs/README/1.4/)
	- [dosemu HOWTO](http://www.dosemu.org/docs/HOWTO/)

#### Misc. BBS stuff
- [Mystic Guy ](https://www.youtube.com/channel/UCPOUHszVXaGNSlK3AdI7kLQ) (Mystic BBS video tutorials, BBS reviews etc.) <br/>
<a href="http://www.youtube.com/watch?v=hidb6Yp0H28"><img src="http://img.youtube.com/vi/hidb6Yp0H28/0.jpg" height="164px" ></a>
- [archives.thebbs.org](http://archives.thebbs.org/) (BBS stuff..)
	- [thebbs.org/dns/](http://thebbs.org/dns/) (thebbs.org dynamic dns service)

