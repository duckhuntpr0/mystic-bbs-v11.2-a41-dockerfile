#!/bin/sh
# Mystic BBS docker boot script

#try to keep docker from tinying the tmux
#rather scale down to 'smallest client'
stty cols 180 rows 60


cd /
if [ ! -f /etc/passwd ] && [ ! -d /home/mysticbbs ]
then
	echo "Extracting /.system.tar.gz"
	tar xvzf --keep-directory-symlink --overwrite --recursive-unlink --unlink-first --same-owner -same-order --atime-preserve /.system.tar.gz /
	chown -R mysticbbs:mysticbbs /home/mysticbbs
fi

#some helpful aliases?
## seems like it would have to be done by ln..
#alias mystic-cfg="cd ~/mystic/ && mystic -cfg"
#alias vim=nvim
#alias duckduckgo="lynx https://duckduckgo.com"

export PS1="$(whoami)@$(hostname) $ "

exec su mysticbbs -c /home/mysticbbs/.start.sh
#[ "$id" = 0 ] && su -l mysticbbs $0 "$@"


# 'tmux -2 -u' .. '-u' is needed for ANSI text in e.g 'mystic -cfg' rendering corretly(?)
