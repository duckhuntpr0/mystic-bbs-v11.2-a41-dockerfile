#/usr/bin/sh
# Mystic BBS start script for user mysticbbs
# /home/mysticbbs/.start.sh
export PS1="$(whoami)@$(hostname) $ "

cd ~

tmux -2 -u \
  new-session -s 'Mystic' -n 'BBS Dashboard' "sleep 2 && /bin/sh ; read" \; \
  set-option -g history-limit 1000 \; \
  split-window -h "sleep 0 && supervisord -n -c ~/.config/supervisord.conf ; read" \; \
  split-window -v "sleep 5 && top ; read" \; \
  select-pane -t 0 \;

