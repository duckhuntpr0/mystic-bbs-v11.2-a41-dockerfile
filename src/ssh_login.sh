#/usr/bin/sh
# Mystic BBS docker ssh login script


if [ $(whoami) == "root" ]
then
	echo "SSH as 'root' user not accepted!"
	exit 126
fi

#first time logging in?
#making sure to set a different password than the default
if [ $(whoami) == "mysticbbs" ] && [ -f /home/mysticbbs/.fresh ]
then
	if [ $(cat /home/mysticbbs/.fresh) -lt 1 ]
	then
		echo "Welcome to your fresh BBS installation!"
		echo 1 > /home/mysticbbs/.fresh
	fi

	if  [ $(cat /home/mysticbbs/.fresh) -lt 2 ]
	then
		echo "Set a new password now!" 
		passwd
		while [ $? -ne 0 ]
		do
			echo "You need to set a new login password NOW!"
			passwd
		done
		if [ $? -eq 0 ]
		then
			echo 2 > /home/mysticbbs/.fresh
		else
			exit 126
		fi
	fi
fi

export PS1="$(whoami)@$(hostname)$ "
tmux a -t Mystic
